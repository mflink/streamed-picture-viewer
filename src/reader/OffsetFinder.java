package reader;

import java.io.IOException;
import java.util.List;

public interface OffsetFinder {
  public int findOffsets(String filename, int maxCount, List<Integer> results) throws IOException;
}
