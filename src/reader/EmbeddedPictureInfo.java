package reader;

public class EmbeddedPictureInfo {
  private String filename;
  private int offset;
  private int width;
  private int height;
  private int colors;
  private int size;

  public EmbeddedPictureInfo(String filename, int offset) {
    this.filename = filename;
    this.offset = offset;
  }

  public String getFilename() {
    return this.filename;
  }

  public int getOffset() {
    return this.offset;
  }

  public int getWidth() {
    return this.width;
  }

  public void setWidth(int width) {
    this.width = width;
  }

  public int getHeight() {
    return this.height;
  }

  public void setHeight(int height) {
    this.height = height;
  }

  public int getColors() {
    return this.colors;
  }

  public void setColors(int colors) {
    this.colors = colors;
  }

  public int getSize() {
    return this.size;
  }

  public void setSize(int size) {
    this.size = size;
  }
}
