package reader;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.IIOException;
import javax.imageio.ImageIO;
import javax.imageio.stream.FileImageInputStream;
import javax.imageio.stream.ImageInputStream;

public class PEFEmbeddedJPEGReader {
  private String filename;

  public PEFEmbeddedJPEGReader(String filename) {
    this.filename = filename;
  }

  public EmbeddedPictureInfo gatherInfoOnly(int offset)
    throws FileNotFoundException, IOException {
    EmbeddedPictureInfo result = null;
    RandomAccessFile reader = new RandomAccessFile(this.filename, "r");


    return result;
  }

  public List<Integer> findOffsetsOnly() throws FileNotFoundException, IOException {
    return PEFEmbeddedJPEGFinder.getInstance().findOffsets(this.filename, -1);
  }

  public BufferedImage load(int offset)
    throws FileNotFoundException, IOException {
    BufferedImage result = null;

    ImageInputStream iis = new FileImageInputStream(new File(this.filename));
    iis.seek(offset);

    try {
      result = ImageIO.read(iis);
    } catch (IIOException iioe) {
    }

    return result;
  }

  public List<BufferedImage> loadAll()
    throws FileNotFoundException, IOException {
    List<Integer> offsets = PEFEmbeddedJPEGFinder.getInstance().findOffsets(this.filename, -1);
    List<BufferedImage> results = new ArrayList<BufferedImage>();

    for (int i = 0; i < offsets.size(); i++) {
      BufferedImage img = this.load(offsets.get(i).intValue());

      if (img != null) {
        results.add(img);
      }
    }

    return results;
  }
}
