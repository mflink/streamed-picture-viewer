package reader;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

public class PEFEmbeddedJPEGFinder extends EmbeddedJPEGFinder {
  //                                               Middle    , Small     , Large
  private final int[] EMBEDDED_STANDARD_OFFSETS = {0x00003394, 0x0001005E, 0x008DF4DE};
  private static PEFEmbeddedJPEGFinder instance;

  /**
   * This class uses Singleton-Design. So the constructor is not accessible.
   * Use the getInstance()-Method for an instance of this class.
   */
  private PEFEmbeddedJPEGFinder() {
  }

  /**
   * @return    An instance of this class.
   */
  public static PEFEmbeddedJPEGFinder getInstance() {
    if (instance == null) {
      instance = new PEFEmbeddedJPEGFinder();
    }

    return instance;
  }

  protected void findOffsetsImplicit(RandomAccessFile reader, int maxCount, List<Integer> results) throws IOException {
    if (reader != null) {
      int data = 0;

      if (results == null) {
        results = new ArrayList<Integer>();
      }

      for (int o = 0; o < this.EMBEDDED_STANDARD_OFFSETS.length; o++) {
        reader.seek(this.EMBEDDED_STANDARD_OFFSETS[o]);
        if (reader.getFilePointer() < reader.length()) {
          data = reader.readInt();

          if (this.isJPEGId(data)) {
            results.add(new Integer(this.EMBEDDED_STANDARD_OFFSETS[o]));

            if (results.size() == maxCount) {
              break;
            }
          }
        }
      }
    }
  }

  /**
   * @param filename    The name of the file to be inspected.
   * @param maxCount    The maximum count of offsets beeing returned. Set less 
   *                    than 0 for all found.
   *
   * @return            A list with the offsets of possible JPEGs found in the
   *                    file.
   *
   * @throws java.io.FileNotFoundException    The file with filename was not
   *                                          found.
   * @throws java.io.IOException              Something happened while touching
   *                                          the file.
   */
  public List<Integer> findOffsets(String filename, int maxCount) throws FileNotFoundException, IOException {
    List<Integer> results = new ArrayList<Integer>();
    RandomAccessFile reader = new RandomAccessFile(filename, "r");

    this.findOffsetsImplicit(reader, maxCount, results);

    if (results.size() == 0) {
      super.findOffsets(filename, maxCount, results);
    }

    reader.close();

    return results;
  }
}
