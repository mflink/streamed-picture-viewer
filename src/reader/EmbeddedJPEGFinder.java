package reader;

import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

public class EmbeddedJPEGFinder implements OffsetFinder {

    private final int EMBEDDED_JPEG_SOI_MARKER = 0xffd80000;
    private final byte[] EMBEDDED_JPEG_SOI_MARKER_BYTES = {(byte) 0xff, (byte) 0xd8};
    private final int EMBEDDED_JPEG_EOI_MARKER = 0xffd90000;
    private final int EMBEDDED_JPEG_ID = 0xffd8ff00;
    private final byte[] EMBEDDED_JPEG_ID_BYTES = {(byte) 0xff, (byte) 0xd8, (byte) 0xff};

    public EmbeddedJPEGFinder() {
    }

    /**
     * @param possibleMarker    This should be at least the first 3 bytes of the
     *                          possible JPEG-Header.
     *
     * @return                  True, if the possibleMarker identifies a possible
     *                          JPEG-Header, false otherwise.
     */
    public boolean isJPEGId(int possibleMarker) {
        return ((possibleMarker & 0xffffff00) == this.EMBEDDED_JPEG_ID);
    }

    /**
     * Calculates the number of bytes the offset can be adjusted to restart search
     * for the EMBEDDED_JPEG_ID pattern.
     *
     * @param data      The data to compare to the pattern.
     *
     * @return          A value of 4 indicates, that this data does not
     *                  contain a part (or the complete) of the pattern.
     *                  A value less than 0 specifies the number of bytes the
     *                  pointer has to be adjusted back to not miss a possible
     *                  match.
     *                  A value of 0 indicates, that the pattern has been found
     *                  completely.
     */
    protected int adjustOffset(int data) {
        int result = 4;

        if (this.isJPEGId(data)) {
            result = 0;
        } else if ((data & 0x00ffffff) == (this.EMBEDDED_JPEG_ID >>> 8)) {
            result = -3;
        } else if ((data & 0x0000ffff) == (this.EMBEDDED_JPEG_ID >>> 16)) {
            result = -2;
        } else if ((data & 0x000000ff) == (this.EMBEDDED_JPEG_ID >>> 24)) {
            result = -1;
        }

        return result;
    }

    private byte[] loadFile(String filename) throws IOException {
        byte[] result = new byte[(int) (new File(filename)).length()];
        RandomAccessFile reader = new RandomAccessFile(filename, "r");

        reader.readFully(result);
        reader.close();

        return result;
    }

    public int findOffsets(RandomAccessFile reader, int maxCount, List<Integer> results) throws IOException {
        int added = 0;

        if (reader != null) {
            int data = 0;
            int adjustment = 0;

            if (results == null) {
                results = new ArrayList<Integer>();
            }

            reader.seek(0);
            data = reader.readInt();
            while (data != -1) {
                if (this.isJPEGId(data)) {
                    // TODO: Validate this header/picture!

                    results.add(new Integer((int) (reader.getFilePointer() - 4)));
                    added++;
                    adjustment = 0;
                } else {
                    adjustment = this.adjustOffset(data);
                }

                if (adjustment < 0) {
                    reader.seek(reader.getFilePointer() + adjustment);
                }

                try {
                    data = reader.readInt();
                } catch (EOFException eofe) {
                    data = -1;
                }
            }
        }

        return added;
    }

    @Override
    public int findOffsets(String filename, int maxCount, List<Integer> results) throws IOException {
        int added = 0;
        byte[] buffer = this.loadFile(filename);
        String sbuffer = new String(buffer);
        int offset = sbuffer.indexOf(new String(this.EMBEDDED_JPEG_ID_BYTES));

        while (offset != -1) {
            results.add(new Integer(offset));
            added++;
        }

        return added;
    }
}
