package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTree;
import javax.swing.WindowConstants;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import reader.PEFEmbeddedJPEGReader;

public class MainGUI extends JFrame implements TreeSelectionListener {
  private String path = "P:/PEF";

  public MainGUI() {
    initComponents();

    this.fileTreeView.addTreeSelectionListener(this);
  }

  @Override
  public void valueChanged(TreeSelectionEvent e) {
    DefaultMutableTreeNode node = (DefaultMutableTreeNode) ((JTree) e.getSource()).getLastSelectedPathComponent();

    if (node.isLeaf()) {
      this.pictureView.clear();
      try {
        String filename = e.getPath().toString().substring(1, e.getPath().toString().length() - 1).replace(", ", "/");
        byte[] b = new byte[(int) (new File(filename)).length()];
        (new RandomAccessFile(filename, "r")).readFully(b);
        String s = new String(b);
        byte[] sb = new byte[] {(byte) 0xff, (byte) 0xd8, (byte) 0xff};
        int o = s.lastIndexOf(new String(sb));
        List<BufferedImage> pictures = (new PEFEmbeddedJPEGReader(filename)).loadAll();

        for (int i = 0; i < pictures.size(); i++) {
          this.pictureView.addPicture(pictures.get(i));
        }
      } catch (FileNotFoundException ex) {
        Logger.getLogger(MainGUI.class.getName()).log(Level.SEVERE, null, ex);
      } catch (IOException ex) {
        Logger.getLogger(MainGUI.class.getName()).log(Level.SEVERE, null, ex);
      }
    } else {
      this.pictureView.clear();
    }
  }

  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    tabs = new JTabbedPane();
    jPanel1 = new JPanel();
    fileTreeView = new FileTreeView(this.path);
    pictureView = new PictureView();

    setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    setTitle("StreamedPictureViewer");

    jPanel1.setLayout(new BorderLayout());

    fileTreeView.setPreferredSize(new Dimension(250, 400));
    jPanel1.add(fileTreeView, BorderLayout.WEST);

    pictureView.setPreferredSize(new Dimension(750, 400));
    jPanel1.add(pictureView, BorderLayout.CENTER);

    tabs.addTab("Betrachter", jPanel1);

    getContentPane().add(tabs, BorderLayout.CENTER);

    pack();
  }// </editor-fold>//GEN-END:initComponents

  // Variables declaration - do not modify//GEN-BEGIN:variables
  private FileTreeView fileTreeView;
  private JPanel jPanel1;
  private PictureView pictureView;
  private JTabbedPane tabs;
  // End of variables declaration//GEN-END:variables
}
