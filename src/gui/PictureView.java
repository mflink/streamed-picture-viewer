package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

public class PictureView extends JPanel {
  private class PictureItem {
    private int index;
    private BufferedImage picture;

    public PictureItem(int index, BufferedImage picture) {
      this.index = index;
      this.setPicture(picture);
    }

    public int getIndex() {
      return this.index;
    }

    public BufferedImage getPicture() {
      return this.picture;
    }

    public void setPicture(BufferedImage picture) {
      this.picture = picture;
    }

    @Override
    public String toString() {
      return this.index + ": " + this.picture.getWidth() + "px * " + this.picture.getHeight() + "px";
    }
  }

  public PictureView() {
    initComponents();
  }

  public void addPicture(BufferedImage picture) {
    if (picture != null) {
      this.pictures.setEnabled(true);
      this.pictures.addItem(new PictureItem(this.pictures.getModel().getSize(), picture));
    }
  }

  public void clear() {
    this.rotateLeft.setEnabled(false);
    this.rotateRight.setEnabled(false);
    this.pictures.setEnabled(false);
    this.view.setIcon(null);
    this.pictures.removeAllItems();
  }

  public BufferedImage getShownPicture() {
    BufferedImage result = null;

    if ((this.pictures.getModel().getSize() > 0) && (this.pictures.getSelectedIndex() > -1)) {
      result = ((PictureItem) this.pictures.getSelectedItem()).getPicture();
    }

    return result;
  }

  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    jScrollPane1 = new JScrollPane();
    view = new JLabel();
    jPanel1 = new JPanel();
    jPanel3 = new JPanel();
    rotateLeft = new JButton();
    rotateRight = new JButton();
    jPanel2 = new JPanel();
    jPanel4 = new JPanel();
    pictures = new JComboBox();

    setLayout(new BorderLayout());

    view.setBackground(Color.black);
    view.setHorizontalAlignment(SwingConstants.CENTER);
    view.setDoubleBuffered(true);
    view.setOpaque(true);
    jScrollPane1.setViewportView(view);

    add(jScrollPane1, BorderLayout.CENTER);

    jPanel1.setLayout(new BoxLayout(jPanel1, BoxLayout.LINE_AXIS));
    jPanel1.add(jPanel3);

    rotateLeft.setText("Links drehen");
    rotateLeft.setEnabled(false);
    rotateLeft.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        rotateLeftActionPerformed(evt);
      }
    });
    jPanel1.add(rotateLeft);

    rotateRight.setText("Rechts drehen");
    rotateRight.setEnabled(false);
    rotateRight.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        rotateRightActionPerformed(evt);
      }
    });
    jPanel1.add(rotateRight);
    jPanel1.add(jPanel2);

    add(jPanel1, BorderLayout.SOUTH);

    jPanel4.setLayout(new BoxLayout(jPanel4, BoxLayout.LINE_AXIS));

    pictures.setEnabled(false);
    pictures.addItemListener(new ItemListener() {
      public void itemStateChanged(ItemEvent evt) {
        picturesItemStateChanged(evt);
      }
    });
    pictures.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        picturesActionPerformed(evt);
      }
    });
    jPanel4.add(pictures);

    add(jPanel4, BorderLayout.PAGE_START);
  }// </editor-fold>//GEN-END:initComponents

  private void rotateLeftActionPerformed(ActionEvent evt) {//GEN-FIRST:event_rotateLeftActionPerformed
    BufferedImage newPicture = new BufferedImage(this.getShownPicture().getHeight(), this.getShownPicture().getWidth(), this.getShownPicture().getType());
    Graphics2D g = (Graphics2D) newPicture.getGraphics();
    AffineTransform at = new AffineTransform();

    at.translate(this.getShownPicture().getHeight() * 0.5, this.getShownPicture().getWidth() * 0.5);
    at.rotate(Math.PI * -0.5);
    at.translate(this.getShownPicture().getWidth() * -0.5, this.getShownPicture().getHeight() * -0.5);

    g.drawRenderedImage(this.getShownPicture(), at);
    g.dispose();

    ((PictureItem) this.pictures.getSelectedItem()).setPicture(newPicture);
    this.pictures.updateUI();
    this.picturesItemStateChanged(null);
}//GEN-LAST:event_rotateLeftActionPerformed

  private void rotateRightActionPerformed(ActionEvent evt) {//GEN-FIRST:event_rotateRightActionPerformed
    BufferedImage newPicture = new BufferedImage(this.getShownPicture().getHeight(), this.getShownPicture().getWidth(), this.getShownPicture().getType());
    Graphics2D g = (Graphics2D) newPicture.getGraphics();
    AffineTransform at = new AffineTransform();

    at.translate(this.getShownPicture().getHeight() * 0.5, this.getShownPicture().getWidth() * 0.5);
    at.rotate(Math.PI * 0.5);
    at.translate(this.getShownPicture().getWidth() * -0.5, this.getShownPicture().getHeight() * -0.5);

    g.drawRenderedImage(this.getShownPicture(), at);
    g.dispose();

    ((PictureItem) this.pictures.getSelectedItem()).setPicture(newPicture);
    this.pictures.updateUI();
    this.picturesItemStateChanged(null);
  }//GEN-LAST:event_rotateRightActionPerformed

  private void picturesItemStateChanged(ItemEvent evt) {//GEN-FIRST:event_picturesItemStateChanged
    if ((this.pictures.getModel().getSize() > 0) && (this.pictures.getSelectedIndex() > -1)) {
      this.view.setIcon(new ImageIcon(((PictureItem) this.pictures.getSelectedItem()).getPicture()));
      this.rotateLeft.setEnabled(true);
      this.rotateRight.setEnabled(true);
    }
}//GEN-LAST:event_picturesItemStateChanged

  private void picturesActionPerformed(ActionEvent evt) {//GEN-FIRST:event_picturesActionPerformed
    // TODO add your handling code here:
}//GEN-LAST:event_picturesActionPerformed
  // Variables declaration - do not modify//GEN-BEGIN:variables
  private JPanel jPanel1;
  private JPanel jPanel2;
  private JPanel jPanel3;
  private JPanel jPanel4;
  private JScrollPane jScrollPane1;
  private JComboBox pictures;
  private JButton rotateLeft;
  private JButton rotateRight;
  private JLabel view;
  // End of variables declaration//GEN-END:variables
}
