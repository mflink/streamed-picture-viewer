package gui;

import java.awt.BorderLayout;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreeSelectionModel;
import utility.SortedDirectoryTree;

public class FileTreeView extends JPanel {
  private SortedDirectoryTree dirTree;
  private String path;

  public FileTreeView() {
    this(null);
  }

  public FileTreeView(String path) {
    initComponents();

    this.treeView.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);

    this.path = path;
    this.dirTree = new SortedDirectoryTree(path, null, null);

    if ((this.path != null) && !this.path.isEmpty()) {
      this.setPath(this.path);
    }
  }

  private void listDirectory(String path, List<String> output) {
    File dir = new File(path);

    if (dir.isDirectory()) {
      String[] files = dir.list();

      for (int i = 0; i < files.length; i++) {
        output.add(path + "/" + files[i]);
        this.listDirectory(path + "/" + files[i], output);
      }
    }
  }

  public void setPath(String path) {
    List<String> directory = new ArrayList<String>();
    this.listDirectory(path, directory);
    String filename = "";

    for (int i = 0; i < directory.size(); i++) {
      filename = directory.get(i).substring(path.length() + 1);

//      if (filename.toLowerCase().endsWith(".pef")) {
        this.dirTree.append(filename);
//      }
    }

    this.path = path;
    this.treeView.setModel(new DefaultTreeModel(this.buildTree(this.dirTree)));
  }

  public String getPath() {
    return this.path;
  }

  private TreeNode buildTree(SortedDirectoryTree root) {
    DefaultMutableTreeNode node = new DefaultMutableTreeNode(root);

    if (!root.getChildren().isEmpty()) {
      Iterator<String> iterator = root.getChildren().keySet().iterator();
      DefaultMutableTreeNode childNode = null;

      while (iterator.hasNext()) {
        node.add((MutableTreeNode) this.buildTree((SortedDirectoryTree) root.getChildren().get(iterator.next())));
      }
    }

    return node;
  }

  public void addTreeSelectionListener(TreeSelectionListener tsl) {
    this.treeView.addTreeSelectionListener(tsl);
  }

  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    jScrollPane1 = new JScrollPane();
    treeView = new JTree();

    setLayout(new BorderLayout());
    DefaultMutableTreeNode treeNode1 = new DefaultMutableTreeNode("root");
    treeView.setModel(new DefaultTreeModel(treeNode1));
    treeView.setRootVisible(false);
    treeView.setShowsRootHandles(true);
    jScrollPane1.setViewportView(treeView);

    add(jScrollPane1, BorderLayout.CENTER);
  }// </editor-fold>//GEN-END:initComponents
  // Variables declaration - do not modify//GEN-BEGIN:variables
  private JScrollPane jScrollPane1;
  private JTree treeView;
  // End of variables declaration//GEN-END:variables

}
