package utility;

import java.util.Map;
import java.util.TreeMap;

public class SortedTree implements Comparable {
  private String id;
  private Object data;
  private SortedTree parent;
  private Map<String, SortedTree> children;

  public SortedTree(String id, Object data, SortedTree parent) {
    this.children = new TreeMap<String, SortedTree>();
    this.id = id;
    this.data = data;
    this.parent = parent;
  }

  public SortedTree getParent() {
    return this.parent;
  }

  public Map<String, SortedTree> getChildren() {
    return this.children;
  }

  public String getId() {
    return this.id;
  }

  public Object getData() {
    return this.data;
  }

  @Override
  public String toString() {
    return this.getId();
  }

  @Override
  public boolean equals(Object o) {
    boolean result = (o == this);

    if (!result && (o instanceof SortedTree)) {
      result = ((SortedTree) o).id.equals(this.id);
    }

    return result;
  }

  @Override
  public int hashCode() {
    return this.id.hashCode();
  }

  @Override
  public int compareTo(Object o) {
    int result = 0;

    if (o != this) {
      result = this.id.compareTo(o.toString());
    } else {
      result = 0;
    }

    return result;
  }
}
