package utility;

public class SortedDirectoryTree extends SortedTree {
  public SortedDirectoryTree(String id, Object data, SortedDirectoryTree parent) {
    super(id, data, parent);
  }

  public void append(String path) {
    if (path.charAt(0) == '/') {
      path = path.substring(1);
    }

    if (!path.isEmpty()) {
      int separatorIndex = path.indexOf('/');

      if (separatorIndex >= 0) {
        String item = path.substring(0, separatorIndex);
        String remaining = "";
        SortedDirectoryTree lastChild = this;

        if (path.length() > (separatorIndex + 1)) {
          remaining = path.substring(separatorIndex + 1);
        }

        if (this.getChildren().containsKey(item)) {
          lastChild = (SortedDirectoryTree) this.getChildren().get(item);
        } else {
          lastChild = new SortedDirectoryTree(item, null, lastChild);
          this.getChildren().put(item, lastChild);
        }

        if (!remaining.isEmpty()) {
          lastChild.append(remaining);
        }
      } else if (!this.getChildren().containsKey(path)) {
        this.getChildren().put(path, new SortedDirectoryTree(path, null, this));
      }
    }
  }
}
