Short name    Bytes     Payload         Name                                     Comments
SOI           0xFFD8    none            Start Of Image 	
SOF0          0xFFC0    variable size 	Start Of Frame (Baseline DCT)            Indicates that this is a baseline DCT-based JPEG, and specifies the width, height, number of components, and component subsampling (e.g., 4:2:0).
SOF2          0xFFC2    variable size 	Start Of Frame (Progressive DCT)         Indicates that this is a progressive DCT-based JPEG, and specifies the width, height, number of components, and component subsampling (e.g., 4:2:0).
DHT           0xFFC4    variable size 	Define Huffman Table(s)                  Specifies one or more Huffman tables.
DQT           0xFFDB    variable size 	Define Quantization Table(s)             Specifies one or more quantization tables.
DRI           0xFFDD    2 bytes         Define Restart Interval                  Specifies the interval between RSTn markers, in macroblocks.
SOS           0xFFDA    variable size 	Start Of Scan                            Begins a top-to-bottom scan of the image. In baseline DCT JPEG images, there is generally a single scan. Progressive DCT JPEG images usually contain multiple scans. This marker specifies which slice of data it will contain, and is immediately followed by entropy-coded data.
RSTn          0xFFDn    variable size 	Restart                                  Inserted every r macroblocks, where r is the restart interval set by a DRI marker. Not used if there was no DRI marker. n, the low 4 bits of the marker code, cycles from 0 to 7.
APPn          0xFFEn    variable size 	Application-specific                     For example, an Exif JPEG file uses an APP1 marker to store metadata, laid out in a structure based closely on TIFF.
COM           0xFFFE    variable size 	Comment                                  Contains a text comment.
EOI           0xFFD9    none            End Of Image 	